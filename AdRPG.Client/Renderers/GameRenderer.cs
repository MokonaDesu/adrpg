﻿using AdRPG.Logic.Game;

namespace AdRPG.Client.Renderers
{
    class GameRenderer
    {
        public static void Render(Game target, EnumViewport viewport = EnumViewport.Fixed)
        {
            System.Console.Clear();
            LocationRenderer.Render(target.CurrentLoc, viewport);
            UIRenderer.Render(target.CurrentPlayer);
        }
    }
}
