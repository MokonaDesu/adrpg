﻿namespace AdRPG.Client.Renderers
{
    struct GenericRenderingData
    {
        public const int X_STARTING = 2;
        public const int Y_STARTING = 2;
        public const byte ATTRIBUTE = 0x0F;
    }
}
