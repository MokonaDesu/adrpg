﻿using AdRPG.Logic.Game;
namespace AdRPG.Client.Renderers
{
    class UIRenderer
    {
        public const int UI_PADDING_Y = 3;
        public const int UI_PADDING_X = 3;
        public const int UI_OFFSET_Y = Program.WINDOW_SIZE_Y - GameConsole.CONSOLE_BUFFER_SIZE - UI_PADDING_Y;
        public const int UI_OFFSET_X = UI_PADDING_X;
        public static void Render(Player player)
        {
            int line = 0;

            foreach (string msg in player.Console.Messages)
            {
                System.Console.SetCursorPosition(UI_OFFSET_X, UI_OFFSET_Y + line++);
                System.Console.Write(msg);
            }
        }

    }
}
