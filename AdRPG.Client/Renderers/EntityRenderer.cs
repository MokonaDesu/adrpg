﻿using AdRPG.Logic.Entities;
using System;

namespace AdRPG.Client.Renderers
{
    class EntityRenderer
    {
        public static void Render(Entity entity, EnumViewport viewport)
        {
            Console.SetCursorPosition(GenericRenderingData.X_STARTING + entity.X, GenericRenderingData.Y_STARTING + entity.Y);

            Console.BackgroundColor = entity.GetBackground();
            Console.ForegroundColor = entity.GetForeground();

            Console.Write(entity.GetDrawing());
        }
    }
}
