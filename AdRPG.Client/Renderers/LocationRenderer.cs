﻿using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;

using System;
using System.Text;

namespace AdRPG.Client.Renderers
{
    class LocationRenderer
    {
        public static void Render(Location loc, EnumViewport viewport)
        {
            for (int y = 0; y < loc.DimensionY; y++)
            {
                Console.SetCursorPosition(GenericRenderingData.X_STARTING, GenericRenderingData.Y_STARTING + y);

                for (int x = 0; x < loc.DimensionX; x++)
                {
                    Tile tile = TileAtlas.Instance.GetById(loc.TileId(x, y));
                    byte meta = loc.TileMetadata(x, y);

                    Console.BackgroundColor = tile.GetBackground(meta);
                    Console.ForegroundColor = tile.GetForeground(meta);
                    Console.Write(tile.GetDrawing(meta));
                }
                
            }

            foreach (var entity in loc.Entities)
            {
                EntityRenderer.Render(entity, viewport);
            }
        }

    }
}
