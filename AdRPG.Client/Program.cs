﻿namespace AdRPG.Client
{
    class Program
    {
        static GameClient _client = new GameClient();

        //public const int WINDOW_SIZE_X = 156;
        //public const int WINDOW_SIZE_Y = 78;

        public const int WINDOW_SIZE_X = 100;
        public const int WINDOW_SIZE_Y = 60;

        static void Main(string[] args)
        {
            System.Console.SetBufferSize(WINDOW_SIZE_X, WINDOW_SIZE_Y);
            System.Console.SetBufferSize(WINDOW_SIZE_X, WINDOW_SIZE_Y);

                System.Console.SetWindowPosition(0, 0);
            _client.RunGameLoop();
        }
    }
}
