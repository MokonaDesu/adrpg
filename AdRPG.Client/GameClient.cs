﻿using AdRPG.Client.Renderers;
using AdRPG.Logic.Game;

namespace AdRPG.Client
{
    enum EnumViewport
    {
        Fixed, FollowingHero
    }

    class GameClient
    {

        private Game _game;

        public GameClient()
        {
            _game = Game.Instance;
        }

        public void RunGameLoop()
        {
            while (true)
            {

                _game.Update();
                GameRenderer.Render(_game);

                while (true)
                {
                    if (_game.ResolveInput()) break; 
                    GameRenderer.Render(_game);
                }
            }
        }

    }
}
