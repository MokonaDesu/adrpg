﻿using System.Collections;

namespace AdRPG.Logic.Game
{
    public class GameConsole
    {
        public const int CONSOLE_BUFFER_SIZE = 10;

        private string[] _messages;
        public IEnumerable Messages
        {
            get { return _messages; }
        }

        public GameConsole()
        {
            _messages = new string[CONSOLE_BUFFER_SIZE];
        }

        public void AddMessage(string message)
        {
            for (int i = 0; i < CONSOLE_BUFFER_SIZE - 1; i++)
            {
                _messages[i] = _messages[i + 1];
            }
            _messages[CONSOLE_BUFFER_SIZE - 1] = message;
        }

    }
}
