﻿using AdRPG.Logic.Entities;

namespace AdRPG.Logic.Game
{
    public class Player
    {
        public string Name { get; set; }
        public EntityPlayer Hero { get; set; }

        public GameConsole Console { get; set; }

        public Player()
        {
            Console = new GameConsole();
        }

    }
}
