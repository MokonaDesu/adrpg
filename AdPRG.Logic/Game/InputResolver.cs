﻿using AdRPG.Logic.World.Loc;
using AdRPG.Logic.Entities;

using System;

namespace AdRPG.Logic.Game
{
    class InputResolver
    {
        public static bool ResolvePlayerInput(Player player)
        {
            bool succeded = false;

            ConsoleKeyInfo pressed = Console.ReadKey(true);
            EnumDirection movingTo = DirectionKey(pressed);
            player.Hero.TryMove(movingTo, out succeded);

            return succeded;
        }

        private static EnumDirection DirectionKey(ConsoleKeyInfo pressed)
        {
            switch (pressed.KeyChar)
            {
                case 'w': return EnumDirection.Up;
                case 'a': return EnumDirection.Left;
                case 's': return EnumDirection.Down;
                case 'd': return EnumDirection.Right;
                default: return EnumDirection.Nowhere;
            }
        }

    }
}
