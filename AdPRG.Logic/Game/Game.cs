﻿using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Generation;
using AdRPG.Logic.World.Loc;

namespace AdRPG.Logic.Game
{
    public class Game
    {
        #region [ Static Data ]

        #endregion

        #region [ Singleton ]
        private static Game _instance;

        public static Game Instance
        {
            get
            {
                return _instance == null ? (_instance = new Game()) : _instance;
            }
        }

        #endregion

        public Location CurrentLoc { get; set; }
        public Player CurrentPlayer { get; set; }

        private Game()
        {
            SetupGenerators();
            SetupPlayer();
            GenerateLoc();
        }

        private void SetupGenerators()
        {
            LocationGeneratorFactory.Instance.RegisterGenerator("dungeon", new DungeonGenerator());
        }

        private void GenerateLoc()
        {
            ILocationGenerator generator = LocationGeneratorFactory.Instance.Generator("default");
            CurrentLoc = generator.Generate();
            CurrentLoc.SpawnEntity(CurrentPlayer.Hero, CurrentLoc.SpawnPositionX, CurrentLoc.SpawnPositionY);
        }

        private void SetupPlayer()
        {
            Player player = new Player();
            player.Hero = new EntityPlayer(player);
            player.Name = "Developer";

            CurrentPlayer = player;
        }

        public void Update()
        {
            CurrentLoc.UpdateEntityList();

            foreach (var entity in CurrentLoc.Entities)
            {
                if (entity is EntityMonster)
                {
                    (entity as EntityMonster).Behavior.Execute(entity);
                }
            }
        }

        public bool ResolveInput()
        {
            return InputResolver.ResolvePlayerInput(CurrentPlayer);
        }
    }
}
