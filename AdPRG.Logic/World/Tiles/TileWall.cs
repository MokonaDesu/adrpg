﻿namespace AdRPG.Logic.World.Tiles
{
    public class TileWall : Tile
    {
        public TileWall(int id) : base(id) { }
        public override char GetDrawing(byte metadata)
        {
            return '█';
        } 

    }
}
