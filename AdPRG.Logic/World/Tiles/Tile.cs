﻿using System;
namespace AdRPG.Logic.World.Tiles
{
    public abstract class Tile
    {
        public int Id { get; protected set; }

        public Tile(int id)
        {
            Id = id;
        }

        public virtual char GetDrawing(byte metadata)
        {
            return ' ';
        }

        public virtual ConsoleColor GetBackground(byte metadata)
        {
            return ConsoleColor.Black;
        }

        public virtual ConsoleColor GetForeground(byte metadata)
        {
            return ConsoleColor.White;
        }

        public virtual bool CanPass(byte metadata)
        {
            return false;
        }

        public virtual bool CanInteract(byte metadata)
        {
            return false;
        }

        public virtual void OnInteract(byte metadata)
        {

        }
    }
}
