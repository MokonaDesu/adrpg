﻿namespace AdRPG.Logic.World.Tiles
{
    class TileVoid : Tile
    {
        public TileVoid(int id) : base(id) { }

        public override bool CanPass(byte metadata)
        {
            return false;
        }

        public override char GetDrawing(byte metadata)
        {
            return ' ';
        }
    }
}
