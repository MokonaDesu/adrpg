﻿using System;

namespace AdRPG.Logic.World.Tiles
{
    public class TileAtlas
    {

        #region [ Singleton ]
        private static TileAtlas _instance;
        public static TileAtlas Instance
        {
            get
            {
                return _instance == null ? (_instance = new TileAtlas()) : _instance;
            }
        }
        #endregion

        #region [ Tiles ]
        public Tile tileVoid = new TileVoid(0);
        public Tile tileAir = new TileAir(1);
        public Tile tileWall = new TileWall(2);
        #endregion

        #region [ Registering Tiles ]
        public void RegisterTiles()
        {
            RegisterTile(tileVoid, tileAir, tileWall);
        }
        #endregion

        private TileAtlas()
        {
            RegisterTiles();
        }

        private Tile[] _tiles = new Tile[128];

        public void RegisterTile(params Tile[] tiles)
        {
            foreach (var tile in tiles)
            {
                if (_tiles[tile.Id] == null)
                {
                    _tiles[tile.Id] = tile;
                }
                else
                {
                    throw new System.ArgumentException("The Atlas already contains a tile with such ID!");
                }
            }
        }

        public Tile GetById(int id)
        {
            return _tiles[id];
        }

    }
}
