﻿namespace AdRPG.Logic.World.Tiles
{
    public class TileAir : Tile
    {
        public TileAir(int id) : base(id) { }

        public override bool CanPass(byte metadata)
        {
            return true;
        }

        public override char GetDrawing(byte metadata)
        {
            return metadata == 0 ? '.' : ',';
        }

    }
}
