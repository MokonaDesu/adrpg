﻿namespace AdRPG.Logic.World.Tiles
{
    class TileDoor : Tile
    {
        public TileDoor(int id) : base(id) { }

        public override char GetDrawing(byte metadata)
        {
            return metadata == 0 ? 'D' : '_';
        }

        public override bool CanPass(byte metadata)
        {
            return metadata != 0;
        }
    }
}
