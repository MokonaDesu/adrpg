﻿using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Generation;

using System.Collections.Generic;

namespace AdRPG.Logic.World.Loc
{
    public class Location
    {
        public readonly int DimensionX;
        public readonly int DimensionY;

        private int[,] _tiles;
        private byte[,] _metadata;
        private IList<Entity> _entities;

        public int SpawnPositionX { get; set; }
        public int SpawnPositionY { get; set; }

        public IEnumerable<Entity> Entities
        {
            get { return _entities; }
        }


        public Location(int xSize, int ySize, int[,] tileMap = null, byte[,] metaMap = null) {
            DimensionX = xSize;
            DimensionY = ySize;
            _tiles = tileMap ?? new int[xSize, ySize];
            _metadata = metaMap ?? new byte[xSize, ySize];
            _entities = new List<Entity>();

            SpawnPositionX = xSize / 2;
            SpawnPositionY = ySize / 2;
        }

        public int TileId(int x, int y)
        {
            return _tiles[x, y];
        }

        public byte TileMetadata(int x, int y)
        {
            return _metadata[x, y];
        }

        public void SetTileId(int x, int y, int Id, byte metadata = 0)
        {
            _tiles[x, y] = Id;
            _metadata[x, y] = metadata;
        }

        public void SetMetadata(int x, int y, byte metadata)
        {
            _metadata[x, y] = 0;
        }

        public void SpawnEntity(Entity entity, int xCoord = 1, int yCoord = 1)
        {
            entity.X = xCoord;
            entity.Y = yCoord;
            entity.Location = this;

            _entities.Add(entity);
        }

        public void UpdateEntityList()
        {
            for (int i = 0; i < _entities.Count; i++)
            {
                if (_entities[i].IsDead)
                {
                    _entities.RemoveAt(i);
                    i--;
                }
            }
        }

    }
}
