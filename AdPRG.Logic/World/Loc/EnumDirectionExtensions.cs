﻿namespace AdRPG.Logic.World.Loc
{
    static class EnumDirectionExtensions
    {
        public static int OffsetX(this EnumDirection direction)
        {
            switch (direction)
            {
                case EnumDirection.Right: return 1;
                case EnumDirection.Left: return -1;
                default: return 0;
            }
        }

        public static int OffsetY(this EnumDirection direction)
        {
            switch (direction)
            {
                case EnumDirection.Up: return -1;
                case EnumDirection.Down: return 1;
                default: return 0;
            }
        }

        public static EnumDirection Opposite(this EnumDirection direction)
        {
            switch (direction)
            {
                case EnumDirection.Down: return EnumDirection.Up;
                case EnumDirection.Up: return EnumDirection.Down;
                case EnumDirection.Left: return EnumDirection.Right;
                case EnumDirection.Right: return EnumDirection.Left;
                default: return EnumDirection.Nowhere;
            }
        }

    }
}