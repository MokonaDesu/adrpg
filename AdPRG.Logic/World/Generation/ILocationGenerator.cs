﻿using AdRPG.Logic.World.Loc;

namespace AdRPG.Logic.World.Generation
{
    public interface ILocationGenerator
    {
        Location Generate();
    }
}
