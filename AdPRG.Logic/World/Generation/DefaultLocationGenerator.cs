﻿using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;

namespace AdRPG.Logic.World.Generation
{
    class DefaultLocationGenerator : ILocationGenerator
    {
        public Location Generate()
        {
            int xSize = 10;
            int ySize = 10;

            int[,] tileMap = new int[xSize, ySize];

            for (int x = 0; x < xSize; x++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    if (x == 0 || x == xSize - 1 || y == 0 || y == ySize - 1)
                    {
                        tileMap[x, y] = TileAtlas.Instance.tileWall.Id;
                    }
                    else tileMap[x, y] = TileAtlas.Instance.tileAir.Id;
                }
            }

            Location loc = new Location(xSize, ySize, tileMap);
            loc.SpawnEntity(new Entities.EntityMonster(), 4, 4);

            return loc;
        }

    }
}
