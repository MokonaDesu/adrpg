﻿using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;
using System;
namespace AdRPG.Logic.World.Generation
{
    class DungeonGenerator : ILocationGenerator
    {

        private Random _generatorRandomizer = new Random();

        public int LocationSizeX { get; set; }
        public int LocationSizeY { get; set; }

        public Location Generate()
        {
            Location loc = new Location(10, 10);
            return loc;
        }

    }
}
