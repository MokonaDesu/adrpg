﻿using System;
using System.Collections.Generic;

namespace AdRPG.Logic.World.Generation
{
    class LocationGeneratorFactory
    {
        #region [ Singleton ]
        private static LocationGeneratorFactory _instance;
        public static LocationGeneratorFactory Instance
        {
            get
            {
                return _instance == null ? (_instance = new LocationGeneratorFactory()) : _instance;
            }
        }
        #endregion

        private IDictionary<string, ILocationGenerator> _registeredGenerators;

        private LocationGeneratorFactory()
        {
            _registeredGenerators = new Dictionary<string, ILocationGenerator>();
            _registeredGenerators.Add(new KeyValuePair<string, ILocationGenerator>("default", new DefaultLocationGenerator()));
        }

        public void RegisterGenerator(string name, ILocationGenerator generator)
        {
            if (!_registeredGenerators.ContainsKey(name))
                _registeredGenerators.Add(new KeyValuePair<string, ILocationGenerator>(name, generator));
            else throw new ArgumentException(string.Format("Generator with such name ({0}) already registered!", name));
        }

        public ILocationGenerator Generator(string name)
        {
            return _registeredGenerators.ContainsKey(name) ? _registeredGenerators[name] : null;
        }
    }
}
