﻿using System;
namespace AdRPG.Logic.World.Generation
{
    public static class RoomSizeExtensions
    {
        public static int RoomSizeTiles(this EnumRoomSize size, Random random)
        {
            switch (size)
            {
                case EnumRoomSize.Small: return random.Next(5, 7);
                case EnumRoomSize.Medium: return random.Next(7, 9);
                case EnumRoomSize.Large: return random.Next(9, 11);
                case EnumRoomSize.Gigantic: return random.Next(11, 13);
                default: return 0;
            }
        }

    }
}
