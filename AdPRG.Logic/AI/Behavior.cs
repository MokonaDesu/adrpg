﻿using AdRPG.Logic.Entities;
namespace AdRPG.Logic.AI
{
    public interface IBehavior
    {
        void Execute(Entity e);

    }
}
