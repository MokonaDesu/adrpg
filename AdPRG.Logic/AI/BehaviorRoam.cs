﻿using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Loc;
using System;
namespace AdRPG.Logic.AI
{
    class BehaviorRoam : IBehavior
    {
        private Random _behaviorRandomizer;

        private float _continuationProbability;
        private float _probabilityDecay = 0.8f;
        
        private EnumDirection _direction;

        public BehaviorRoam() {
            _behaviorRandomizer = new Random();
            ShuffleBehavior();
        }

        public void Execute(Entity entity)
        {
            if (TestForContinuation())
            {
                _continuationProbability *= _probabilityDecay;
            }
            else
            {
                ShuffleBehavior();
            }

            bool result = false;
            entity.TryMove(_direction, out result);

            if (!result)
            {
                _continuationProbability = 0f;
                this.Execute(entity);
            }
        }

        private void ShuffleBehavior()
        {
            PickDirection();
            _continuationProbability = 1f;
        }

        private bool TestForContinuation()
        {
            return _behaviorRandomizer.NextDouble() <= _continuationProbability;
        }

        private void PickDirection()
        {
            _direction = (EnumDirection)_behaviorRandomizer.Next(4);
        }    

    }
}
