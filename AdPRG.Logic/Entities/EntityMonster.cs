﻿using AdRPG.Logic.AI;
namespace AdRPG.Logic.Entities
{
    class EntityMonster : Entity
    {
        public IBehavior Behavior
        {
            get;
            protected set;
        }
        
        public EntityMonster()
        {
            Health = 7;
            Behavior = new BehaviorRoam();
        }

        public override char GetDrawing()
        {
            return 'M';
        }
    }
}
