﻿using AdRPG.Logic.World.Loc;
using System;

namespace AdRPG.Logic.Entities
{
    public abstract class Entity
    {
        public Location Location { get; set; }

        #region [ Positioning ]
        private int _x;
        public int X
        {
            get { return _x; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Location coordinate value can't be negative");

                _x = value;
            }
        }

        private int _y;
        public int Y
        {
            get { return _y; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Location coordinate value can't be negative");

                _y = value;
            }
        }

        public EnumDirection Facing
        {
            get;
            set;
        }
        #endregion

        #region [ Health ]
        private int _health;

        public int Health
        {
            get { return _health; }
            set
            {
                _health = value < 0 ? 0 : value;
            }
        }

        public bool IsDead
        {
            get
            {
                return Health <= 0;
            }
        }

        public void Kill()
        {
            Health = 0;
        }

        public void Damage(int strength)
        {
            Health -= strength;
        }
        #endregion

        #region [ Abstract Properties ]

        public virtual char GetDrawing()
        {
            return 'E';
        }

        public virtual ConsoleColor GetBackground()
        {
            return ConsoleColor.Black;
        }

        public virtual ConsoleColor GetForeground()
        {
            return ConsoleColor.White;
        }

        #endregion

    }
}
