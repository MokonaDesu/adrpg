﻿using AdRPG.Logic.Game;
namespace AdRPG.Logic.Entities
{
    public class EntityPlayer : Entity
    {
        public Player Player { get; protected set; }

        public EntityPlayer(Player player)
        {
            Health = 20;
            Player = player;
        }

        public override char GetDrawing()
        {
            return 'P';
        }

    }
}
